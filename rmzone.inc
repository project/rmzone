<?php
/**
 * @file
 * Provides API functions for identifying which Royal Mail world zone a country
 * falls under. Uses ISO 3166-1 alpha-2 country codes.
 *
 * @see includes/iso.inc
 * @see http://www.royalmail.com/worldzones
 * @see http://www.iso.org/iso/iso-3166-1_decoding_table
 */

/**
 * Tests if a country is in the UK.
 */
function rmzone_country_in_uk($country) {
  $countries = rmzone_countries_uk();
  return isset($countries[$country]);
}

/**
 * Tests if a country is in Europe.
 */
function rmzone_country_in_europe($country) {
  $countries = rmzone_countries_europe();
  return isset($countries[$country]);
}

/**
 * Tests if a country is in world zone 1.
 */
function rmzone_country_in_world_zone_1($country) {
  $countries = rmzone_countries_world_zone_1();
  return isset($countries[$country]);
}

/**
 * Tests if a country is in world zone 2.
 */
function rmzone_country_in_world_zone_2($country) {
  $countries = rmzone_countries_world_zone_2();
  return isset($countries[$country]);
}

/**
 * Tests if a country is in world zone 3.
 */
function rmzone_country_in_world_zone_3($country) {
  $countries = rmzone_countries_world_zone_3();
  return isset($countries[$country]);
}

/**
 * Gets the zone for a country.
 *
 * @param $country
 *   The country to get the zone for.
 *
 * @return
 *  The zone the country is in. One of:
 *   - 'uk'
 *   - 'europe'
 *   - 'world zone 1'
 *   - 'world zone 2'
 *   - 'world zone 3'
 */
function rmzone_country_get_zone($country) {
  if (rmzone_country_in_uk($country)) {
    return 'uk';
  }
  if (rmzone_country_in_europe($country)) {
    return 'europe';
  }
  if (rmzone_country_in_world_zone_1($country)) {
    return 'world zone 1';
  }
  if (rmzone_country_in_world_zone_2($country)) {
    return 'world zone 2';
  }
  // If none of the above apply, then we're in zone 3.
  return 'world zone 3';
}

/**
 * Tests if a country's in a zone.
 *
 * @param $country
 *   The country to test
 * @param $zone
 *   The Royal Mail zone to test against; can be
 *   - uk
 *   - europe
 *   - world zone 1
 *   - world zone 2
 *   - world zone 3
 */
function rmzone_country_in_zone($country, $zone) {

  switch ($zone) {
    case 'uk':
      return rmzone_country_in_uk($country);

    case 'europe':
      return rmzone_country_in_europe($country);

    case 'world zone 1':
      return rmzone_country_in_world_zone_1($country);

    case 'world zone 2':
      return rmzone_country_in_world_zone_2($country);

    case 'world zone 3':
      return rmzone_country_in_world_zone_3($country);
  }

  return FALSE;
}

/**
 * Returns a list of countries in the Royal Mail zone: UK.
 */
function rmzone_countries_uk() {
  return drupal_map_assoc(array('GB'));
}

/**
 * Returns a list of countries in the Royal Mail zone: Europe.
 */
function rmzone_countries_europe() {
  return drupal_map_assoc(array(

    'AL', // Albania
    'AD', // Andorra
    'AM', // Armenia
    'AT', // Austria
    'AZ', // Azerbaijan
    // Belearic Islands: Spain
    'BY', // Belarus
    'BE', //Belgium
    'BA', // Bosnia Herzegovina
    'BG', // Bulgaria
    // Canary Islands: Spain
    // Corsica's French
    'HR', // Croatia
    'CY', // Cyprus
    'CZ', // Czech Republic

    'DK', // Denmark
    'EE', // Estonia
    'FO', // Faroe Islands
    'SF', // Finland
    'FR', // France
    'GE', // Georgia
    'DE', // Germany
    'GI', // Gibraltar
    'GR', // Greece
    'GL', // Greenland
    'HU', // Hungary
    'IS', // Iceland
    'IE', // Irish Republic
    'IT', // Italy
    'KZ', // Kazakhstan
    // Kosovo doesn't have an agreed code, and Drupal's iso.inc doesn't include
    // it.

    'KG', // Kyrgyzstan
    'LV', // Latvia
    'LI', // Liechtenstein
    'LT', // Lithuania
    'LU', // Luxembourg
    'MK', // Macedonia
    // Madeira: Portugal
    'MT', // Malta
    'MD', // Moldova
    'MC', // Monaco
    'ME', // Montenegro
    'NL', // Netherlands
    'NO', // Norway
    'PL', // Poland
    'PT', // Portugal
    'RO', // Romania

    'RU', // Russia
    'SM', // San Marino
    'RS', // Serbia
    'SK', // Slovakia
    'SI', // Slovenia
    'ES', // Spain
    'SE', // Sweden
    'CH', // Switzerland
    'TJ', // Tajikistan
    'TR', // Turkey
    'TM', // Turkmenistan
    'UA', // Ukraine
    'UZ', // Uzbekistan
    'VA', // Vatican City State

  ));
}

/**
 * Returns a list of countries in the Royal Mail zone: World zone 1. Note that
 * this zone is defined as being anything which isn't any of the other zones.
 */
function rmzone_countries_world_zone_1() {
  include_once DRUPAL_ROOT . '/includes/iso.inc';
  $all_countries = drupal_map_assoc(array_keys(_country_get_predefined_list()));

  $other_countries =
    rmzone_countries_world_zone_2() +
    rmzone_countries_world_zone_3() +
    rmzone_countries_europe() +
    rmzone_countries_uk();

  return array_diff($all_countries, $other_countries);
}

/**
 * Returns a list of countries in the Royal Mail zone: World zone 2. Note that
 * there seem to be two islands that are part of the Norwegian Antarctic
 * Territory that don't have ISO 3166-1 codes: Peter I Island and Queen Maud
 * Land.
 *
 * @see http://en.wikipedia.org/wiki/Dependent_territory#Norway
 * @see http://www.iso.org/iso/iso-3166-1_decoding_table
 */
function rmzone_countries_world_zone_2() {
  return drupal_map_assoc(array(

    'AU', // Australia
    'PW', // Belau
    'IO', // British Indian Ocean Territory
    'CX', // Christmas Island (Indian Ocean)
    // Christmas Island (Pacific Ocean)
    'CC', // Cocos Islands
    'CK', // Cook Island
    // Coral Sea Island: Australia
    'FJ', // Fiji
    'PF', // French Polynesia
    'TF', // French South Antarctic Territory
    // Keeling: Cocos Islands
    'KI', // Kiribati
    'MO', // Macao
    'NR', // Nauru Island
    'NC', // New Caledonia
    'NZ', // New Zealand
    // New Zealand Antarctic Territory: New Zealand
    'NU', // Niue Island
    'NF', // Norfolk Island
    'BV', // Norwegian Antarctic Territory (NB this excludes Peter I Island and
    // Queen Maud Land, which don't have ISO codes - see
    // http://en.wikipedia.org/wiki/Dependent_territory#Norway).
    'PG', // Papua New Guinea
    'LA', // People's Democratic Republic of Laos
    'PN', // Pitcairn Island
    'SG', // Republic of Singapore
    'SB', // Solomon Islands
    // Tahiti: French Polynesia
    'TK', // Tokelau Island
    'TO', // Tonga
    'TV', // Tuvalu
    'AS', // US Samoa
    'WS', // Western Samoa

  ));
}

/**
 * Returns a list of countries in the Royal Mail zone: World Zone 3.
 *
 * This zone was added in July 2020 and contains only the USA (US).
 */
function rmzone_countries_world_zone_3() {
  return drupal_map_assoc(array('US'));
}
