<?php
/**
 * @file
 * Provides Rules conditions and supplementary functions for Royal Mail's zones.
 */

/**
 * Implements hook_rules_condition_info().
 */
function rmzone_rules_condition_info() {

  // General purpose condition that just takes a text input.
  $conditions['rmzone_condition_country_in_zone'] = array(
    'group' => t("Royal Mail"),
    'label' => t("Country is in Royal Mail zone"),
    'parameter' => array(
      'country' => array(
        'type' => 'text',
        'label' => t("Country"),
        'default mode' => 'selector',
      ),
      'zone' => array(
        'type' => 'text',
        'label' => t("Zone"),
        'description' => t("The <a href=\"!world_zone_link\">Royal Mail zone</a> you want to test against.", array('!world_zone_link' => url('http://www.royalmail.com/worldzones'))),
        'options list' => 'rmzone_options_zones',
      ),
    ),
    'callbacks' => array(
      'execute' => 'rmzone_condition_country_in_zone',
    ),
  );

  // Commerce specific condition that takes an order and address field name.
  if (module_exists('commerce_order')) {
  
    $conditions['rmzone_condition_order_country_in_zone'] = array(
      'group' => t("Royal Mail"),
      'label' => t("Order address is in Royal Mail zone"),
      'parameter' => array(
        'commerce_order' => array(
          'type' => 'commerce_order',
          'label' => t("Order"),
          'description' => t("The order containing the profile reference with the address in question."),
        ),
        'address_field' => array(
          'type' => 'text',
          'label' => t('Address'),
          'options list' => 'rmzone_options_address_field_list',
          'description' => t('The address associated with this order whose country you want to check.'),
          'restriction' => 'input',
        ),
        'zone' => array(
          'type' => 'text',
          'label' => t("Zone"),
          'description' => t("The <a href=\"!world_zone_link\">Royal Mail zone</a> you want to test against.", array('!world_zone_link' => url('http://www.royalmail.com/worldzones'))),
          'options list' => 'rmzone_options_zones',
        ),
      ),
      'callbacks' => array(
        'execute' => 'rmzone_condition_order_country_in_zone',
      ),
    );

  }
  
  return $conditions;
}

/**
 * Rules condition callback: Tests if $country is in $zone.
 */
function rmzone_condition_country_in_zone($country, $zone) {
  module_load_include('inc', 'rmzone');
  return rmzone_country_in_zone($country, $zone);
}

/**
 * Rules condition callback: Tests if the $address_field field of $order is in
 * $zone.
 */
function rmzone_condition_order_country_in_zone($order, $address_field, $zone) {
  module_load_include('inc', 'rmzone');
  
  $country = _rmzone_get_country_from_order_and_address_field($order, $address_field);
  if ($country) {
    return rmzone_country_in_zone($country, $zone);
  }
  
  return FALSE;
}

/**
 * Rules options list callback: returns addressfields attached to
 * commerce_order.
 *
 * @see commerce_order_address_field_options_list()
 */
function rmzone_options_address_field_list() {
  module_load_include('inc', 'commerce_order', 'commerce_order.rules');
  return commerce_order_address_field_options_list();
}

/**
 * Rules options list callback: Returns a list of Royal Mail's zones.
 *
 * @see http://www.royalmail.com/worldzones
 */
function rmzone_options_zones() {
  return array(
    'uk' => t("UK"),
    'europe' => t("Europe"),
    'world zone 1' => t("World zone 1"),
    'world zone 2' => t("World zone 2"),
    'world zone 3' => t("World zone 3"),
  );
}

/**
 * Helper function that returns the country from the $address_field field of
 * $order.
 *
 * @return
 *   The ISO 3166-1 alpha-2 country code if found; the empty string otherwise.
 *
 * @see commerce_order_rules_compare_address()
 */
function _rmzone_get_country_from_order_and_address_field($order, $address_field) {
  list($field_name, $address_field_name) = explode('|', $address_field);

  // If we actually received a valid order...
  if (!empty($order)) {
    $wrapper = entity_metadata_wrapper('commerce_order', $order);

    // And if we can actually find the requested address data...
    if (!empty($wrapper->{$field_name}) && !empty($wrapper->{$field_name}->{$address_field_name})) {
      return $wrapper->{$field_name}->{$address_field_name}->country->value();
    }
  }
  
  return '';
}
