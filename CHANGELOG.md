## [1.0-rc1]
### Changed
 - US is now in World Zone 3: [From 1 July 2020][0] Royal Mail [have introduced
   a new world zone for the US][1].

[0]: https://web.archive.org/web/20200830132846/https://www.themailingroom.com/royal-mail-adds-world-zone-3/
[1]: https://www.royalmail.com/international-zones#worldzone3

[Unreleased]: https://git.drupalcode.org/project/rmzone/-/compare/7.x-1.0-rc1...7.x-1.x
[1.0-rc1]: https://git.drupalcode.org/project/rmzone/-/compare/7.x-1.0-beta2...7.x-1.0-rc1
