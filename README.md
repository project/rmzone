# Royal Mail zones

The [UK's Royal Mail][1] categorises destinations as falling into five zones
across the [world][2]: the UK, Europe, and world zones 1, 2 and 3. The Royal
Mail zones module makes it easy to check which zone a country's in.

This module doesn't
- integrate with any Royal Mail APIs (if there are any);
- provide Royal Mail shipping pricing rules.

## Rules

There are two Rule conditions provided:

1. **Country is in Royal Mail zone**: This takes the country and zone as text
   inputs.

2. **Order address is in Royal Mail zone**: This takes the country, a Commerce
   Order, and the address you're interested in (eg. shipping). Requires
   [Commerce][3].

## API

All countries are specified as [two letter ISO 3166-1 alpha-2 country codes][4]
(eg. GB). The following test functions all accept a country code and return a
boolean value.

```php
module_load_include('inc', 'rmzone');
rmzone_country_in_uk($country);
rmzone_country_in_europe($country);
rmzone_country_in_world_zone_1($country);
rmzone_country_in_world_zone_2($country);
rmzone_country_in_world_zone_3($country);
rmzone_country_in_zone($country, $zone);
```

The following function returns the zone of the given country:

```php
module_load_include('inc', 'rmzone');
rmzone_country_get_zone($country);
```

The returned zone is one of:
  - `uk`
  - `europe`
  - `world zone 1`
  - `world zone 2`
  - `world zone 3`

The following functions all return an array with matching key value pairs of 
country codes.

```php
module_load_include('inc', 'rmzone');
rmzone_countries_uk();
rmzone_countries_europe();
rmzone_countries_world_zone_1();
rmzone_countries_world_zone_2();
rmzone_countries_world_zone_3();
```

## Installation & Use

1. Install the module [normally][5].
2. The Rules conditions will now be available for use.
3. If you'd like to use the API directly, you must add this line first:
   `module_load_include('inc', 'rmzone');`

[1]: http://www.royalmail.com/
[2]: https://www.royalmail.com/sending/international/country-guides
[3]: http://drupal.org/project/commerce
[4]: https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements
[5]: http://drupal.org/node/895232
